import os
import pickle
import pytest
from datetime import datetime
from sklearn.metrics import precision_score
from sklearn.datasets import load_breast_cancer
from sklearn.model_selection import train_test_split

RANDOM_STATE=123

def data(*, test_size=0.3):
    X, y = load_breast_cancer(return_X_y=True)
    return train_test_split(X, y, test_size=test_size, random_state=RANDOM_STATE)

@pytest.fixture(scope='module')
def get_model_data(model_path='../my_model.pkl'):
    _, X_test, _, y_test = data()
    assert os.path.exists(model_path), 'Nothing to test!'
    with open(model_path, 'rb') as file:
        model = pickle.load(file)
    return model, X_test, y_test

def test_comptime(get_model_data):
    model, X_test, y_test = get_model_data
    t1 = datetime.now()
    model.predict(X_test)
    t2 = datetime.now()
    mean_time = (datetime.timestamp(t2) - datetime.timestamp(t1)) / X_test.shape[0]
    print('Mean time per sample: ', mean_time)
    assert mean_time < 1e-3

def test_precision(get_model_data):
    model, X_test, y_test = get_model_data
    preds = model.predict(X_test)
    score = precision_score(y_test, preds)
    print('Precision: ', score)
    assert score > 0.8


