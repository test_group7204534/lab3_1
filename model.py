from sklearn.datasets import load_breast_cancer
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
import pickle

RANDOM_STATE = 123
np.random.seed(RANDOM_STATE)

GRID = {
    'n_estimators': [10, 50, 100],
    'max_depth': [5, 10, 20]
}


def data(*, test_size=0.3):
    X, y = load_breast_cancer(return_X_y=True)
    return train_test_split(X, y, test_size=test_size, random_state=RANDOM_STATE)


def get_model(X_train, y_train, save='../my_model.pkl', scoring = 'f1_weighted'):
    gridsearch = GridSearchCV(RandomForestClassifier(random_state=RANDOM_STATE), GRID,
                              scoring=scoring, cv=3).fit(X_train, y_train)
    print(f'Greatest {scoring} reached: {gridsearch.best_score_}\n')
    if save:
        with open(save, 'wb') as file:
            pickle.dump(gridsearch.best_estimator_, file)
    return gridsearch.best_estimator_


def predict(model, X_test, fpath='predictions.txt'):
    predictions = model.predict(X_test)
    np.savetxt(fpath, predictions, delimiter=",")
    return predictions

def plot(y_true, y_pred, fpath='result.png'):
    fig, ax = plt.subplots(figsize=(5, 5))
    cm = confusion_matrix(y_true, y_pred)
    im = ax.imshow(cm)

    ax.set_xticks([0, 1])
    ax.set_yticks([0, 1])
    for i in range(2):
        for j in range(2):
            text = ax.text(j, i, cm[i, j],
                           ha="center", va="center", color="w")

    fig.tight_layout()
    # plt.show()
    fig.savefig(fpath)
    plt.savefig('tmp.jpg')

if __name__ == '__main__':
    X_train, X_test, y_train, y_test = data()
    model = get_model(X_train, y_train)
    print('Start prediction\n')
    t1 = datetime.timestamp(datetime.now())
    predictions = predict(model, X_test)
    t2 = datetime.timestamp(datetime.now())
    print(f'Mean estimation time {(t2 - t1) / X_test.shape[0]}\n')
    plot(y_test, predictions, fpath='../result.png')

    fig, ax = plt.subplots(figsize=(5, 5))
    cm = confusion_matrix(y_test, predictions)
    im = ax.imshow(cm)

    ax.set_xticks([0, 1])
    ax.set_yticks([0, 1])
    for i in range(2):
        for j in range(2):
            text = ax.text(j, i, cm[i, j],
                           ha="center", va="center", color="w")
    plt.savefig('result.png')
